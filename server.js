
/*
Importer les dépendances
*/
    // Composants
    const express = require('express');
    const path = require('path');
    const ejs = require('ejs');

    // Modules
    const frontRoute = require('./routes/front');
//


// Instantiation de l'Arduino
'use strict';
const {
    EtherPortClient
} = require('etherport-client');
var five = require('johnny-five'),
    button, button2, button3, button4;
var board = new five.Board({
    port: new EtherPortClient({
        host: '192.168.43.180',
        port: 3030
    }),
    repl: false
});

    // Configurer le serveur
    const app = express();
    const port = process.env.PORT || 3000;
    var server = require("http").createServer(app);

    let io = require('socket.io').listen(server);
    
    board.on("ready", function () {
        console.log("La carte est initialisée.");

        io.on('connection', function (socket) {
            console.log('a user connected');
        
            button = new five.Button(13);
            button2 = new five.Button(14);
            button3 = new five.Button(0);
            button4 = new five.Button(12);

            //   board.repl.inject({
            //     button: button
            //   });
        
            button.on("down", function () {
                console.log("down1");
                socket.emit('right', "");
            });

            button2.on("down", function () {
                console.log("down2");
                socket.emit('up', "");
            });

            button3.on("down", function () {
                console.log("down3");
                socket.emit('down', "");
            });

            button4.on("down", function () {
                console.log("down4");
                socket.emit('left', "");
            });

            

            // button2.on("down", function () {
            //     console.log("down2");
            //     socket.emit('hello', "Hello, can you hear me ?");
            // });
        });
    });


    // Configurer le dossier des vues client
    app.set( 'views', __dirname + '/www' );
    app.use( express.static(path.join(__dirname, 'www')) );

    // Définir le moteur de rendu
    // app.engine( 'html', ejs.renderFile );
    app.set( 'view engine', 'ejs' );

    // Configurer les routes
    app.use('/', frontRoute);
//

/*
Lancer le serveur
*/
    server.listen(port, () => console.log('server is on' + port))
//

// const LED_PIN = 6;
console.log("Le programme est lancé.");
